#include <iostream>
#include <cmath>
#include "ponto2D.h"

using namespace std;

int currentId = 0;

//inline int Ponto2D::getNextId(){
// { //if true, criacao. if false, destruicao
//     if (true)
//         currentId++;
//     else
//         currentId--;
//     return currentId;
// }

inline double Ponto2D::getx() {return x;}
inline double Ponto2D::gety() { return y; }
inline double Ponto2D::getid() { return id; }


inline double Ponto2D::setx(double xnew) { x = xnew;}
inline double Ponto2D::sety(double ynew) { y = ynew;}

// contrutor default - inicializa o ponto2D com as coordenadas x = 0.0 e y = 0.0;
inline Ponto2D::Ponto2D()
{
    x = 0.0;
    y = 0.0;
    //id =  getNextId(true);
}

// contrutor parametrico - inicializa o ponto2D com as coordenadas x = px e y = py;
inline Ponto2D::Ponto2D(double px, double py)
{
    x = px;
    y = py;
    //id = getNextId(true);
}

// destrutor
inline Ponto2D::~Ponto2D()
{
    //getNextId(false); //libera o id
}

// Função que calcula a distância entre dois pontos
inline double Ponto2D::distTo(const Ponto2D p1, const Ponto2D p2)
{
    double somaDoQuadradoDosCatetos;
    somaDoQuadradoDosCatetos = pow(abs(p1.x - p2.x), 2) + pow(abs(p1.y - p2.y), 2);
    return sqrt(somaDoQuadradoDosCatetos);
}

//Função que calcula a distância entre a origem e um ponto
inline double Ponto2D::distTo(const Ponto2D p1)
{
    double somaDoQuadradoDosCatetos;
    somaDoQuadradoDosCatetos = pow(abs(p1.x), 2) + pow(abs(p1.y), 2);
    return sqrt(somaDoQuadradoDosCatetos);
}

//Função redefine o valor do objeto p1 somando-o a p2   
inline void Ponto2D::sumOf(const Ponto2D p2){
     this->x = this->x + p2.x;
     this->y = this->y + p2.y;
}

// //Função que soma o valor do objeto p1 a p2 e retorna em p3   
// // inline double Ponto2D::sumOf(const Ponto2D p1){
//     double xValue = this->x + p2.x;
//     double yValue = this->y + p2.y;
//     Ponto2D p3(xValue, yValue);
//     return p3; 
// // }