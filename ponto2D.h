#include <iostream>

using namespace std;


class Ponto2D
{
private:
    double x; // x e o valor da cordenada x no plano cartesiano
    double y; // y e o valor da cordenada y no plano cartesiano
    int id;   // identificador do ponto

    int getNextId(bool isNew);
    int getNextId();

public:
    

    // Construtores
    Ponto2D();
    Ponto2D(double x, double y);

    // Destrutor
    ~Ponto2D();


    // basic getters
    double getx();
    double gety();
    double getid();

    // basic setters
    double setx(double x);
    double sety(double y);
    
    //
    double distTo(Ponto2D p1);
    double distTo(Ponto2D p1, Ponto2D p2);
    
    void sumOf(Ponto2D p2);
    // double sumOf(Ponto2D p1);

    // other methods
    void print() const;
    // void sumOf(Ponto2D p1, Ponto2D p2);
};